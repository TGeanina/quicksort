/* Author: Geanina Tambaliuc */
#include <iostream>

using namespace std;

//for testing purposes I limited the size of the vector v to 100
// n is the actual size of the vector
int v[100], n;

/*
This function selects the first element as pivot.
Then it places all the smaller elements than the pivot to the left side,
and the larger elements than the pivot to the right side.

p - starting index
u - ending index
*/
int poz(int p, int u)
{

    int piv=v[p]; //the pivot

    //put smaller elements to the left, and larger to the right
    while(p<u)
    {

        if(v[p]>v[u])
        {

            int aux=v[p];
            v[p]=v[u];
            v[u]=aux;
        }
        if(piv==v[p]) u=u-1;
        else p=p+1;
    }
    return p;
}

/*
 The recursive QuickSort function.

 p - starting index;
 u - ending index;
*/
void quicksort(int p,int u)
{

    if(p<u)
    {
        int k=poz(p,u);
        quicksort(p,k-1);
        quicksort(k+1,u);
    }
}
int main()
{
    cout<<"Enter n: ";
    cin>>n;
    cout<<"Enter the n numbers: ";
    for(int i=0;i<n;i++)
        cin>>v[i];

    quicksort(0,n-1); //executing the Quick Sort function

    for(int i=0;i<n;i++)
        cout<<v[i]<<" ";
    return 0;
}
